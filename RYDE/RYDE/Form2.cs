﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RYDE
{
    public partial class Form2 : Form
    {
        //data type and variables
        public Form2(double dist, String from, string to, int rad)
        {
            InitializeComponent();
            double total = 2.50 +( dist * 0.81) + 1.75;
            // radio button checking that pool or direct
            if (rad == 1)
            {
                label1.Text = "RYDE POOL IS CONFIRMED!!";
            }

            if (rad ==2)
            {
                label2.Text = "RYDE DIRECT IS CONFIRMED!!";
            }
            // assign label to variable for displaying result

            label2.Text = "your Pickup location is: " + from;
            label3.Text = "your Dropoff location is: " + to;
            label4.Text = "Booking fee $" + 2.50;
            label5.Text = "Distance charge $" + (dist * 0.81);
            label6.Text = "Service fee $" + 1.75;
            
            // for increasing the fair if direct pool

            if ( rad == 2)
            {
                total = 2.50 + (2.50 * 0.1) + (dist * 0.81) + 0.15 + (dist * 0.81) + 1.75;
            }
            // getting time for peek hours(10am to 12am, 4pm to 6pm, 8pm to 9pm)
            TimeSpan time = DateTime.Now.TimeOfDay;
            if((time >= new TimeSpan(10, 0, 0)) && (time <= new TimeSpan(12,0,0)) ||
                (time >= new TimeSpan(16, 0, 0)) && (time <= new TimeSpan(18, 0, 0)) ||
                (time >= new TimeSpan(20, 0, 0)) && (time <= new TimeSpan(21, 0, 0))) 
                
              {
                total = total + (0.2 * total);
              }
            // checking less price then make it minimum
            if ( total < 5.50)
            {
                total = 5.50;
            }

            label7.Text = "Total Ryde charge is & " + total;
            
        }
    }
}
